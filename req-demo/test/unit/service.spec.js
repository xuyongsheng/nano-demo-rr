"use strict";

const { ServiceBroker } = require("moleculer");
const { ValidationError } = require("moleculer").Errors;
const TestService = require("../../services/req.service");

describe("Test 'greeter' service", () => {
	let broker = new ServiceBroker();
	broker.createService(TestService);

	beforeAll(() => broker.start());
	afterAll(() => broker.stop());

	describe("Test 'greeter.hello' action", () => {

		it("should return with 'Hello Moleculer'", () => {
			expect(broker.call("req.hello")).resolves.toBe("Hello Moleculer");
		});

	});
	
	describe("Test 'greeter.welcome' action", () => {

	});

});

