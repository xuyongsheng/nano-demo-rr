"use strict";

module.exports = {
	name: "rsp",

	/**
	 * Default settings
	 */
	settings: {

	},

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Say a 'Hello'
		 * 
		 * @returns 
		 */
		hello() {
			return "Hello Moleculer";
		},

		/**
		 * Welcome a username
		 * 
		 * @param {String} name - User name
		 */
		done: {
            params: {
                from: 'string'
            },
			handler(ctx) {
				console.log(ctx.params.from)

				return `good job`;
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}	
};