"use strict";

module.exports = {
	name: "req",

	/**
	 * Default settings
	 */
	settings: {

	},

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Say a 'Hello'
		 * 
		 * @returns 
		 */
		hello() {
			return "Hello Moleculer";
		},

		/**
		 * Welcome a username
		 * 
		 * @param {String} name - User name
		 */
		do: {
			async handler(ctx) {
				const data = await ctx.call('rsp.done', {from : 'req.do'})

				return `done, ${data}`;
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}	
};